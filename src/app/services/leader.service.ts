import { Injectable } from '@angular/core';
import { Leader } from '../shared/leader';
import { Observable, of } from 'rxjs';
import { baseURL } from '../shared/baseurl';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map , catchError} from 'rxjs/operators';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class LeaderService {

  constructor(private http: HttpClient,
    private processhttpmsgservice : ProcessHTTPMsgService) { }

  getLeaders(): Observable<Leader[]> {
    return this.http.get<Leader[]>(baseURL+'leadership')
    .pipe(catchError(this.processhttpmsgservice.handleError));
  }

  getLeader( id: string) : Observable<Leader> {
    return this.http.get<Leader>(baseURL+'leadership/'+ id)
    .pipe(catchError(this.processhttpmsgservice.handleError));
  } 

  getFeaturedLeader(): Observable<Leader> {
    return this.http.get<Leader>(baseURL+'leadership?featured=true')
    .pipe(map(leader => leader[0]))
    .pipe(catchError(this.processhttpmsgservice.handleError));
  }
}
