import { Injectable } from '@angular/core';
import {Promotion} from '../shared/promotion';
import { Observable,of } from 'rxjs';
import { baseURL } from '../shared/baseurl';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map , catchError} from 'rxjs/operators';
import { ProcessHTTPMsgService } from './process-httpmsg.service';


@Injectable({
  providedIn: 'root'
})
export class PromotionService {

  constructor(private http : HttpClient,
    private processhttpmsgservice : ProcessHTTPMsgService) { }

  getPromotions() : Observable<Promotion[]> {
    return this.http.get<Promotion[]>(baseURL + 'promotions')
    .pipe(catchError(this.processhttpmsgservice.handleError));
  }

  getPromotion(id: String): Observable<Promotion>{
    return this.http.get<Promotion>(baseURL + 'promotions' + id)
    .pipe(catchError(this.processhttpmsgservice.handleError));
  }

  getFeaturedPromotion():Observable<Promotion> {
    return this.http.get<Promotion>(baseURL + 'promotions?featured=true')
    .pipe(map(promotion => promotion[0]))
    .pipe(catchError(this.processhttpmsgservice.handleError));
  }
}
