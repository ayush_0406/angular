import { Component, OnInit, Input, ViewChild,Inject} from '@angular/core';
import { Dish } from '../shared/dish';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DishService } from '../services/dish.service';
import { switchMap } from 'rxjs/operators';
import { FormBuilder , FormGroup , Validators } from '@angular/forms';
import { Comments } from '../shared/Commentform';
import { Comment } from '../shared/comment';
import { visibility,flyInOut, expand } from '../animations/app.animation';

@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss'],
  host: {
    '[@flyInOut]': 'true',
    'style': 'display: block;'
 },
 animations: [
   flyInOut(),
   visibility(),
   expand()
 ]
})
export class DishdetailComponent implements OnInit {

   dish: Dish;
   dishIds: string[];
   prev: string;
   next: string;
   commentform : FormGroup;
   comments: Comments;
   addedcmt: Comment;
   @ViewChild('cform') commentformDirective;
   errMess: string;
   dishcopy: Dish;
   visibility ='shown';

   fomErrors={
    'comment': '',
    'author': '' 
   };

   validationMessages={
    'comment': {
      'required': 'Comment is required.'
    },
    'author': {
      'required': 'Name is required.',
      'minlength': 'Name should contain atleast 2 characters.'
    }
   };


  constructor(private dishservice: DishService,
    private route: ActivatedRoute,
    private location: Location,
    private fc:FormBuilder,
    @Inject('BaseURL') private BaseURL) { }

  ngOnInit() {
    this.createForm();
    this.dishservice.getDishIds().subscribe((dishIds) => this.dishIds= dishIds);
    this.route.params.pipe(switchMap((params: Params) => { this.visibility = 'hidden'; return this.dishservice.getDish(params['id']); }))
    .subscribe(dish => { this.dish = dish; this.dishcopy = dish; this.setPrevNext(dish.id); this.visibility= 'shown'; },
    errmess => this.errMess = <any>errmess );
  }

  setPrevNext(dishId: string) {
    const index= this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1 )% this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1 )% this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }

  createForm(): void {
    this.commentform = this.fc.group({
      rating: 5,
      comment: ['', Validators.required],
      author: ['', [Validators.required,Validators.minLength(2)]]
    });

    this.commentform.valueChanges.subscribe(data => this.onvalueChanged(data));
    this.onvalueChanged(); // (re) set Form validation messages
  }

  onvalueChanged(data?: any){
    if (!this.commentform) {return ;}
    const form = this.commentform;
    for ( const field in this.fomErrors ) {
      if(this.fomErrors.hasOwnProperty(field)) {
        this.fomErrors[field] = '';
        const control = form.get(field);
        if(control && control.dirty && !control.valid){
          const messages= this.validationMessages[field];
          for (const key in control.errors){
            if(control.errors.hasOwnProperty(key)){
              this.fomErrors[field] += messages[key]+ ' ';
            }
          }
        }
      }
    }
  }

  onSubmit(){
    this.comments=this.commentform.value;
    console.log(this.comments);
    this.addedcmt=this.commentform.value;
    var d = new Date();
    this.addedcmt.date=d.toISOString();
    this.dishcopy.comments.push(this.addedcmt);
    this.dishservice.putDish(this.dishcopy)
    .subscribe(dish => {
      this.dish = dish ; this.dishcopy = dish;
    },
    errmess => {this.dish = null ; this.dishcopy = null ; this.errMess = <any>errmess;});
    this.commentform.reset({
      rating:5,
      comment: '' ,
      author: ''
    }); 
    this.commentformDirective.resetForm({rating: 5});
  }
}

